/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/json"
	"os"
	"strconv"

	"github.com/tidwall/gjson"

	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/log"

	resourcebindingv1alpha1 "gitlab.com/bitspur/rock8s/resource-binding-operator/api/v1alpha1"
)

// ResourceBindingReconciler reconciles a ResourceBinding object
type ResourceBindingReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=resourcebinding.rock8s.com,resources=resourcebindings,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=resourcebinding.rock8s.com,resources=resourcebindings/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=resourcebinding.rock8s.com,resources=resourcebindings/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the ResourceBinding object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *ResourceBindingReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log.FromContext(ctx)

	resourceBinding := &resourcebindingv1alpha1.ResourceBinding{}
	err := r.Get(ctx, req.NamespacedName, resourceBinding)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	unstructuredObj := &unstructured.Unstructured{}
	unstructuredObj.SetAPIVersion(resourceBinding.Spec.Resource.APIVersion)
	unstructuredObj.SetKind(resourceBinding.Spec.Resource.Kind)
	unstructuredObj.SetName(resourceBinding.Spec.Resource.Name)
	unstructuredObj.SetNamespace(resourceBinding.Namespace)
	err = r.Get(ctx, client.ObjectKeyFromObject(unstructuredObj), unstructuredObj)
	if err != nil {
		condition := metav1.Condition{
			Type:    "Failed",
			Status:  metav1.ConditionTrue,
			Reason:  "GetFailed",
			Message: err.Error(),
		}
		meta.SetStatusCondition(&resourceBinding.Status.Conditions, condition)
		if err := r.Status().Update(ctx, resourceBinding); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if resourceBinding.Spec.Status != nil {
		resourceBinding.Status.Resource = make(map[string]string)
		jsonObj, err := json.Marshal(unstructuredObj.Object)
		if err != nil {
			return ctrl.Result{}, err
		}
		jsonStr := string(jsonObj)
		allFound := true
		for key, valuePath := range resourceBinding.Spec.Status {
			if valuePath != "" {
				value := gjson.Get(jsonStr, valuePath)
				if value.Exists() {
					resourceBinding.Status.Resource[key] = value.String()
				} else {
					allFound = false
				}
			}
		}
		if !allFound {
			return ctrl.Result{Requeue: true}, nil
		}
	}

	readyCondition := meta.FindStatusCondition(resourceBinding.Status.Conditions, "Ready")
	if readyCondition != nil && readyCondition.Status == metav1.ConditionTrue {
		return ctrl.Result{}, nil
	}
	resourceBinding.Status.ObservedGeneration = resourceBinding.Generation
	ownerReference := metav1.OwnerReference{
		APIVersion: resourceBinding.Spec.Resource.APIVersion,
		Kind:       resourceBinding.Spec.Resource.Kind,
		Name:       resourceBinding.Spec.Resource.Name,
		UID:        resourceBinding.UID,
	}
	resourceBinding.Status.OwnerReferences = []metav1.OwnerReference{ownerReference}
	condition := metav1.Condition{
		Type:    "Ready",
		Status:  metav1.ConditionTrue,
		Reason:  "ResourceBound",
		Message: "resource bound successfully",
	}
	resourceBinding.Status.Conditions = []metav1.Condition{}
	meta.SetStatusCondition(&resourceBinding.Status.Conditions, condition)
	if err := r.Status().Update(ctx, resourceBinding); err != nil {
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ResourceBindingReconciler) SetupWithManager(mgr ctrl.Manager) error {
	maxConcurrentReconciles := 3
	if value := os.Getenv("MAX_CONCURRENT_RECONCILES"); value != "" {
		if val, err := strconv.Atoi(value); err == nil {
			maxConcurrentReconciles = val
		}
	}
	return ctrl.NewControllerManagedBy(mgr).
		WithOptions(controller.Options{MaxConcurrentReconciles: maxConcurrentReconciles}).
		For(&resourcebindingv1alpha1.ResourceBinding{}).
		Complete(r)
}
