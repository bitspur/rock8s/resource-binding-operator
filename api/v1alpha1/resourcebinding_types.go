/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type ResourceBindingSpecResource struct {
	metav1.TypeMeta `json:",inline"`

	// resource name
	Name string `json:"name,omitempty"`
}

// ResourceBindingSpec defines the desired state of ResourceBinding
type ResourceBindingSpec struct {
	Resource ResourceBindingSpecResource `json:"resource,omitempty"`

	// a mapping of values from the bound resource to the status output
	Status map[string]string `json:"status,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// ResourceBinding is the Schema for the resourcebindings API
type ResourceBinding struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ResourceBindingSpec   `json:"spec,omitempty"`
	Status ResourceBindingStatus `json:"status,omitempty"`
}

// ResourceBindingStatus defines the observed state of ResourceBinding
type ResourceBindingStatus struct {
	Resource           map[string]string       `json:"resource,omitempty"`
	ObservedGeneration int64                   `json:"observedGeneration,omitempty"`
	Conditions         []metav1.Condition      `json:"conditions,omitempty"`
	OwnerReferences    []metav1.OwnerReference `json:"ownerReferences,omitempty"`
}

//+kubebuilder:object:root=true

// ResourceBindingList contains a list of ResourceBinding
type ResourceBindingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ResourceBinding `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ResourceBinding{}, &ResourceBindingList{})
}
